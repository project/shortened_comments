README.txt
==========

INTRODUCTION
------------

This module shorten comments on any content type. In the permissions user
roles can be chosen, which means that shortened comments will be displayed 
for them. 
Also in setting number of characters or words can be chosen. There is a option 
to display a custom text underneath the shortened comment.

REQUIREMENTS
==========
Installation of Drupal 7.
This module requires the following core modules:
* Field
* Comment


INSTALLATION
==========
Download the module and simply copy it into your contributed modules folder:
[for example, your_drupal_path/sites/all/modules] and enable it from the
modules administration/management page.

CONFIGURATION
-------------
* Configure user permissions in Administration » People » Permissions:
* At http://[site]/admin/structure/types/manage/[content-type]/comment/display
click the format and choose Shortened Comment. From the option choose 
How many characters should be displayed
OR
How many words should be displayed

Only one option will work, if you leave both, then the first option will be
only available.
Leave empty field to ignore it.
The Text to be shown for shortened comments can include HTML tags.

TROUBLESHOOTING
---------------
If the shortened comments do not display, check the user permissions.

MAINTAINERS
-----------
* Stefan Kimak (stefank) https://www.drupal.org/user/2444106
